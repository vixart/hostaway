<?php

use App\Routers\PhoneBookRoutes;
use Phalcon\Mvc\Router\Group as RouterGroup;
use Phalcon\Mvc\Router;

$router = new Router(false);
$router->notFound(
    [
        'controller' => 'index',
        'action'     => 'notFound',
    ]
);
$router->mount(new PhoneBookRoutes());
$invoices = new RouterGroup();

$di->set('router', $router);