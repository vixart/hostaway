<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
    ]
);

$loader->registerNamespaces(
    [
        'App\Routers' => 'app/routers',
        'App\Models' => 'app/models',
        'App\Validations' => 'app/validations',
        'App\Resources\Transformers' => 'app/resources/transformers',
        'App\Resources\Serializers' => 'app/resources/serializers',
        'App\Resources' => 'app/resources',
        'App\External\Hostaway' => 'app/external/hostaway',
        'App\Repositories' => 'app/repositories',
        'App\Requests' => 'app/requests',
    ]
);

$loader->register();
