<?php
declare(strict_types=1);

use Phalcon\Http\Response;

class IndexController extends ControllerBase
{

    public function notFoundAction()
    {
        $this->view->disable();
        $response = new Response();
        $response->setStatusCode(404);
        $response->setJsonContent(null, JSON_PRETTY_PRINT);
        $response->send();
    }

}

