<?php
declare(strict_types=1);

use App\Repositories\PhoneBookRepository;
use App\Requests\PhoneBookFindRequest;
use App\Requests\PhoneBookListRequest;
use App\Requests\PhoneBookStoreRequest;
use App\Requests\PhoneBookUpdateRequest;
use App\Resources\PhoneBookResource;
use Phalcon\Http\Response;
use Phalcon\Mvc\Controller;

class PhoneBookController extends Controller
{
    private Response $response;
    private PhoneBookResource $phoneBookResource;
    private PhoneBookRepository $phoneBookRepository;
    private PhoneBookListRequest $phoneBookListRequest;
    private PhoneBookFindRequest $phoneBookFindRequest;
    private PhoneBookStoreRequest $phoneBookStoreRequest;
    private PhoneBookUpdateRequest $phoneBookUpdateRequest;

    public function initialize()
    {
        $this->view->disable();
        $this->response = $this->container->get('response');
        $this->phoneBookResource = $this->container->get('phone_book_resource');
        $this->phoneBookRepository = $this->container->get('phone_book_repository');
        $this->phoneBookListRequest = $this->container->get('phone_book_list_request');
        $this->phoneBookFindRequest = $this->container->get('phone_book_find_request');
        $this->phoneBookStoreRequest = $this->container->get('phone_book_store_request');
        $this->phoneBookUpdateRequest = $this->container->get('phone_book_update_request');
    }

    /**
     * Retrieve filtered list of a phone books
     */
    public function listAction()
    {
        $filter = $this->phoneBookListRequest->validate()->asArray();
        $response = $this->phoneBookRepository->list($filter);
        $this->response->setJsonContent($this->phoneBookResource->toArray($response), JSON_PRETTY_PRINT);
        $this->response->send();
    }

    /**
     * Retrieve single phone book by id
     */
    public function showAction()
    {
        $id = $this->phoneBookFindRequest->validate()->getId();
        $data = $this->phoneBookRepository->findById($id);
        $this->response->setJsonContent($this->phoneBookResource->toItem($data), JSON_PRETTY_PRINT);
        $this->response->send();
    }

    /**
     * Store a phone book
     */
    public function storeAction()
    {
        $data = $this->phoneBookStoreRequest->validate()->asArray();
        $this->phoneBookRepository->store($data);
        $this->response->setJsonContent(null, JSON_PRETTY_PRINT);
        $this->response->send();
    }

    /**
     * Delete a phone book
     */
    public function deleteAction()
    {
        $id = $this->phoneBookFindRequest->validate()->getId();
        $this->phoneBookRepository->delete($id);
        $this->response->setJsonContent(null, JSON_PRETTY_PRINT);
        $this->response->send();
    }

    /**
     * Update a phone book
     */
    public function updateAction()
    {
        $data = $this->phoneBookUpdateRequest->validate()->asArray();
        $id = $this->phoneBookUpdateRequest->getId();
        $this->phoneBookRepository->update($id, $data);
        $this->response->setJsonContent(null, JSON_PRETTY_PRINT);
        $this->response->send();
    }
}

