<?php
declare(strict_types=1);

namespace App\Routers;

use Phalcon\Mvc\Router\Group as RouterGroup;

class PhoneBookRoutes extends RouterGroup
{
    public function initialize()
    {
        $this->setPrefix('/phone-book');
        $this->add(
            '/:int',
            [
                'controller' => 'PhoneBook',
                'action' => 'show',
                'id' => 1
            ]
        )->via(['GET']);

        $this->add(
            '/',
            [
                'controller' => 'PhoneBook',
                'action' => 'list'
            ]
        )->via(['GET']);

        $this->add(
            '/',
            [
                'controller' => 'PhoneBook',
                'action' => 'store'
            ]
        )->via(['POST']);

        $this->add(
            '/:int',
            [
                'controller' => 'PhoneBook',
                'action' => 'update',
                'id' => 1
            ]
        )->via(['PUT', 'PATCH']);

        $this->add(
            '/:int',
            [
                'controller' => 'PhoneBook',
                'action' => 'delete',
                'id' => 1
            ]
        )->via(['DELETE']);
    }
}