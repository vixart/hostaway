<?php

namespace App\Validations;

use App\Resources\ErrorResource;
use Phalcon\Http\Response;
use Phalcon\Validation;

abstract class ApiValidation extends Validation
{
    /**
     * Method handles errors after validations
     * @param $data
     * @param $entity
     * @param $messages
     */
    public function afterValidation($data, $entity, $messages)
    {
        if (count($messages)) {
            $errorResource = new ErrorResource();
            $response = new Response();
            $response->setStatusCode(400);
            $response->setJsonContent($errorResource->toArray($messages), JSON_PRETTY_PRINT);
            $response->send();
            die;
        }
    }
}