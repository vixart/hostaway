<?php

namespace App\Validations;

use Phalcon\Validation\Validator\Regex;

class PhoneBookFilterValidation extends ApiValidation
{
    public function initialize()
    {
        $this->add(
            'name',
            new Regex(
                [
                    'message' => 'The name parameter must contain only letters and spaces',
                    'pattern' => '/[a-zA-Z\s]+/',
                    'allowEmpty' => true,
                ]
            )
        );
    }
}