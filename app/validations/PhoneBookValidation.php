<?php

namespace App\Validations;

use App\External\Hostaway\HostawayCountries;
use App\External\Hostaway\HostawayTimezones;
use Phalcon\Validation\Validator\Alpha;
use Phalcon\Validation\Validator\Callback;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;

class PhoneBookValidation extends ApiValidation
{
    private HostawayCountries $hostawayCountries;
    private HostawayTimezones $hostawayTimezones;

    public function initialize()
    {
        $this->hostawayCountries = $this->di->get('hostaway_countries');
        $this->hostawayTimezones = $this->di->get('hostaway_timezones');

        $this->add(
          'first_name',
            new PresenceOf(
                [
                    'message' => 'First name is required',
                ]
            )
        );

        $this->add(
            'first_name',
            new Alpha(
                [
                    'message' => 'First name must contain only symbols'
                ]
            )
        );

        $this->add(
            'last_name',
            new Alpha(
                [
                    'message' => 'Last name must contain only symbols'
                ]
            )
        );

        $this->add(
            'phone_number',
            new PresenceOf(
                [
                    'message' => 'Phone number is required',
                ]
            )
        );

        $this->add(
            'phone_number',
            new Regex(
                [
                    'message' => 'Phone number must contain numbers and start with +',
                    'pattern' => '/\+[0-9]+/',
                ]
            )
        );

        $this->add(
            'country_code',
            new StringLength(
                [
                    "max"             => 2,
                    "min"             => 2,
                    "messageMaximum"  => "Country code must be 2 characters long",
                    "messageMinimum"  => "Country code must be 2 characters long",
                    "includedMaximum" => false,
                    "includedMinimum" => false,
                    'allowEmpty' => true,
                ]
            )
        );

        $this->add(
            'country_code',
            new Alpha(
                [
                    'message' => 'Country code must contain only letters',
                ]
            )
        );

        $this->add(
            'country_code',
            new Callback(
                [
                    'message' => 'Country code must be valid according to https://api.hostaway.com/countries',
                    'callback' => function($data) {
                        $countryCodes = $this->hostawayCountries->get()->codesResponse();
                        if(in_array($data['country_code'], $countryCodes)) {
                            return true;
                        }

                        return false;
                    },
                    'allowEmpty' => true
                ]
            )
        );

        $this->add(
            'timezone_name',
            new Callback(
                [
                    'message' => 'Timezone must be valid according to https://api.hostaway.com/timezones',
                    'callback' => function($data) {
                        $timezoneNames = $this->hostawayTimezones->get()->namesResponse();
                        if(in_array($data['timezone_name'], $timezoneNames)) {
                            return true;
                        }

                        return false;
                    },
                    'allowEmpty' => true
                ]
            )
        );
    }
}