<?php

namespace App\Validations;

use App\Models\PhoneBook;
use Phalcon\Validation\Validator\Callback;

class PhoneBookExistValidation extends ApiValidation
{
    public function initialize()
    {
        $this->add(
          'id',
            new Callback(
                [
                    'message' => 'This record not exist',
                    'callback' => function($data) {
                        $phoneBook = PhoneBook::findFirst($data['id']);
                        if($phoneBook) {
                            return true;
                        }
                        return false;
                    }
                ]
            )
        );
    }
}