<?php

namespace App\Resources;

use App\Resources\Serializers\ErrorSerializer;
use App\Resources\Transformers\ErrorTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Phalcon\Messages\Messages;

class ErrorResource
{
    private Manager $manager;

    public function __construct()
    {
        $this->manager = new Manager();
        $this->manager->setSerializer(new ErrorSerializer());
    }

    /**
     * Return array data for API errors
     * @param Messages $errors
     * @return array
     */
    public function toArray(Messages $errors)
    {
        $resource = new Collection($errors, new ErrorTransformer);
        return $this->manager->createData($resource)->toArray();
    }
}