<?php

namespace App\Resources\Transformers;

use League\Fractal\TransformerAbstract;
use Phalcon\Messages\Message;

class ErrorTransformer extends TransformerAbstract
{
    /**
     * Transform data for API errors
     * @param Message $data
     * @return array
     */
    public function transform(Message $data): array
    {
        return [
            'field' => $data->getField(),
            'message' => $data->getMessage()
        ];
    }
}