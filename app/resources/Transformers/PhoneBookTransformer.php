<?php

namespace App\Resources\Transformers;

use App\Models\PhoneBook;
use League\Fractal\TransformerAbstract;

class PhoneBookTransformer extends TransformerAbstract
{
    /**
     * Transform data for phone book entity
     * @param PhoneBook $phoneBook
     * @return array
     */
    public function transform(PhoneBook $phoneBook): array
    {
        return [
            "id" => $phoneBook->id,
            "first_name" => $phoneBook->first_name,
            "last_name" => $phoneBook->last_name,
            "phone_number" => $phoneBook->phone_number,
            "country_code" => $phoneBook->country_code,
            "timezone_name" => $phoneBook->timezone_name,
            "inserted_on" => $phoneBook->inserted_on,
            "updated_on" => $phoneBook->updated_on,
        ];
    }
}