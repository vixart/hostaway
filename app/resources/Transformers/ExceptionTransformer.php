<?php

namespace App\Resources\Transformers;

use Exception;
use League\Fractal\TransformerAbstract;

class ExceptionTransformer extends TransformerAbstract
{
    /**
     * Transform data for application's exceptions
     * @param Exception $e
     * @return array
     */
    public function transform(Exception $e): array
    {
        return [
            'code' => $e->getCode(),
            'message' => $e->getMessage()
        ];
    }
}