<?php

namespace App\Resources;

use App\Resources\Transformers\PhoneBookTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\DataArraySerializer;

class PhoneBookResource
{
    private Manager $manager;
    public function __construct()
    {
        $this->manager = new Manager();
        $this->manager->setSerializer(new DataArraySerializer());
    }

    /**
     * Return phone book as entity
     * @param $data
     * @return array
     */
    public function toItem($data)
    {
        $resource = new Item($data, new PhoneBookTransformer());
        return $this->manager->createData($resource)->toArray();
    }

    /**
     * Return phone books as array
     * @param $data
     * @return array
     */
    public function toArray($data)
    {
        $resource = new Collection($data, new PhoneBookTransformer());
        return $this->manager->createData($resource)->toArray();
    }
}