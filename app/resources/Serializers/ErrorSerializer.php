<?php

namespace App\Resources\Serializers;

use League\Fractal\Pagination\CursorInterface;
use League\Fractal\Pagination\PaginatorInterface;
use League\Fractal\Resource\ResourceInterface;
use League\Fractal\Serializer\SerializerAbstract;

class ErrorSerializer extends SerializerAbstract
{
    public function collection($resourceKey, array $errors)
    {
        return ['errors' => $errors];
    }

    public function item($resourceKey, array $errors)
    {
        return ['errors' => $errors];
    }

    public function null()
    {
        return ['errors' => []];
    }

    public function includedData(ResourceInterface $resource, array $errors)
    {
        return [];
    }

    public function meta(array $meta)
    {
        return [];
    }

    public function paginator(PaginatorInterface $paginator)
    {
        return [];
    }

    public function cursor(CursorInterface $cursor)
    {
        return [];
    }
}