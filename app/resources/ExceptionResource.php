<?php

namespace App\Resources;

use App\Resources\Serializers\ErrorSerializer;
use App\Resources\Transformers\ExceptionTransformer;
use Exception;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ExceptionResource
{
    private Manager $manager;

    public function __construct()
    {
        $this->manager = new Manager();
        $this->manager->setSerializer(new ErrorSerializer());
    }

    /**
     * Return array data for application's exceptions
     * @param Exception $e
     * @return array
     */
    public function toArray(Exception $e)
    {
        $resource = new Collection([$e], new ExceptionTransformer());
        return $this->manager->createData($resource)->toArray();
    }
}