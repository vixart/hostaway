<?php

namespace App\External\Hostaway;

use Exception;

class HostawayTimezones extends HostawayApi
{
    public function __construct()
    {
        parent::__construct();
        $this->setUri('/timezones');
        $this->setMethod('GET');
        $this->setBody("");
    }

    /**
     * Get timezones from hostaway.com
     * @throws Exception
     */
    public function get()
    {
        $this->sendRequest();
        return $this;
    }

    /**
     * Extracts names from response
     * @return array
     */
    public function namesResponse(): array
    {
        $response = json_decode($this->rawResponse());
        return array_keys(get_object_vars($response->result));
    }
}