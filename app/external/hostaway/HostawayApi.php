<?php

namespace App\External\Hostaway;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;

abstract class HostawayApi
{
    private string $baseUri;
    protected Client $client;
    private array $headers;
    private string $body;
    private string $method;
    private string $uri;
    private string $response;

    public function __construct(float $timeout = 20.0)
    {
        $this->baseUri = 'https://api.hostaway.com/';
        $this->headers = ['Content-Type' => 'application/json'];
        $this->response = "";
        $this->client = new Client([
            'base_uri' => $this->baseUri,
            'timeout' => $timeout
        ]);
    }

    /**
     * Add header to request
     * @param string $key
     * @param string $value
     */
    protected function addHeader(string $key, string $value): void
    {
        $this->headers[$key] = $value;
    }

    /**
     * Sets request's body
     * @param string $body
     */
    protected function setBody(string $body): void
    {
        $this->body = $body;
    }

    /**
     * Sets request's method
     * @param string $method
     */
    protected function setMethod(string $method): void
    {
        $this->method = $method;
    }

    /**
     * Sets request's URI
     * @param string $uri
     */
    protected function setUri(string $uri): void
    {
        $this->uri = $uri;
    }

    /**
     * Return request's URI
     * @return string
     */
    protected function getUri(): string
    {
        return $this->uri;
    }

    /**
     * Return raw data from response
     * @return string
     */
    public function rawResponse(): string
    {
        return $this->response;
    }

    /**
     * Sends request
     * @throws Exception
     */
    protected function sendRequest(): void
    {
        try {
            $request = new Request($this->method, $this->uri, $this->headers, $this->body);
            $this->response = $this->client->send($request)->getBody()->getContents();
        } catch (GuzzleException $e) {
            throw new Exception();
        }
    }
}