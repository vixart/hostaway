<?php

namespace App\External\Hostaway;

use Exception;

class HostawayCountries extends HostawayApi
{
    public function __construct()
    {
        parent::__construct();
        $this->setUri('/countries');
        $this->setMethod('GET');
        $this->setBody("");
    }

    /**
     * Retrieve countries from hostaway.com
     * @throws Exception
     */
    public function get(): HostawayCountries
    {
        $this->sendRequest();
        return $this;
    }

    /**
     * Extracts codes from response
     * @return array
     */
    public function codesResponse(): array
    {
        $response = json_decode($this->rawResponse());
        return array_keys(get_object_vars($response->result));
    }
}