<?php

namespace App\Repositories;

use App\Models\PhoneBook;
use Phalcon\Mvc\Model\ResultsetInterface;

class PhoneBookRepository
{
    /**
     * Fields that could be filed
     * @var array
     */
    private array $fillable = ["first_name", "last_name", "phone_number", "country_code", "timezone_name"];

    /**
     * Retrieve list of phone books
     * @param array $filter
     * @return PhoneBook|PhoneBook[]|ResultsetInterface
     */
    public function list(array $filter)
    {
        return PhoneBook::find(
            [
                "CONCAT(first_name, ' ', last_name) LIKE '%{$filter['name']}%'"
            ]
        );
    }

    /**
     * Return single phone book by ID
     * @param int $id
     * @return PhoneBook
     */
    public function findById(int $id): PhoneBook
    {
        return PhoneBook::findFirst($id);
    }

    /**
     * Store phone book
     * @param array $data
     */
    public function store(array $data): void
    {
        $phoneBook = new PhoneBook();
        $phoneBook->assign($data, $this->fillable);
        $phoneBook->save();
    }

    /**
     * Delete phone book
     * @param int $id
     */
    public function delete(int $id): void
    {
        $phoneBook = PhoneBook::findFirst($id);
        $phoneBook->delete();
    }

    /**
     * Update phone book
     * @param int $id
     * @param array $data
     */
    public function update(int $id, array $data): void
    {
        $phoneBook = PhoneBook::findFirst($id);
        $phoneBook->assign($data, $this->fillable);
        $phoneBook->update();
    }
}