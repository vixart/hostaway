<?php

namespace App\Requests;

use App\Validations\PhoneBookExistValidation;
use App\Validations\PhoneBookValidation;
use Phalcon\Di\Injectable;

class PhoneBookUpdateRequest extends Injectable
{
    private array $request;
    private PhoneBookValidation $phoneBookValidator;
    private PhoneBookExistValidation $phoneBookExistValidator;

    public function __construct()
    {
        $this->phoneBookValidator = $this->di->get('phone_book_validator');
        $this->phoneBookExistValidator = $this->di->get('phone_book_exist_validator');
    }

    /**
     * Validate phone book update request
     * @return PhoneBookUpdateRequest
     */
    public function validate(): PhoneBookUpdateRequest
    {
        $this->request = json_decode(file_get_contents("php://input"), true);
        $id = $this->dispatcher->getParam('id', 'int');

        $this->phoneBookExistValidator->validate(['id' => $id]);
        $this->phoneBookValidator->validate($this->request);

        return $this;
    }

    /**
     * Return phone book update data
     * @return array
     */
    public function asArray(): array
    {
        return [
            "first_name" => $this->request['first_name'],
            "last_name" => $this->request['last_name'],
            "phone_number" => $this->request['phone_number'],
            "country_code" => $this->request['country_code'],
            "timezone_name" => $this->request['timezone_name']
        ];
    }

    /**
     * Return phone book ID for update
     * @return int
     */
    public function getId(): int
    {
        return $this->dispatcher->getParam('id', 'int');
    }
}