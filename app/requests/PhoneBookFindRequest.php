<?php

namespace App\Requests;

use App\Validations\PhoneBookExistValidation;
use Phalcon\Di\Injectable;

class PhoneBookFindRequest extends Injectable
{
    private PhoneBookExistValidation $phoneBookExistValidator;

    public function __construct()
    {
        $this->phoneBookExistValidator = $this->di->get('phone_book_exist_validator');
    }

    /**
     * Validate phone books id in database
     * @return PhoneBookFindRequest
     */
    public function validate(): PhoneBookFindRequest
    {
        $id = $this->dispatcher->getParam('id', 'int');
        $this->phoneBookExistValidator->validate(['id' => $id]);
        return $this;
    }

    /**
     * Return ID from URL params
     * @return int
     */
    public function getId(): int
    {
        return $this->dispatcher->getParam('id', 'int');
    }
}