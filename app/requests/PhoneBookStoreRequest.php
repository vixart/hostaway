<?php

namespace App\Requests;

use App\Validations\PhoneBookExistValidation;
use App\Validations\PhoneBookValidation;
use Phalcon\Di\Injectable;

class PhoneBookStoreRequest extends Injectable
{
    private PhoneBookValidation $phoneBookValidator;

    public function __construct()
    {
        $this->phoneBookValidator = $this->di->get('phone_book_validator');
    }

    /**
     * Validate phone book store request
     * @return PhoneBookStoreRequest
     */
    public function validate(): PhoneBookStoreRequest
    {
        $this->phoneBookValidator->validate($_POST);
        return $this;
    }

    /**
     * Return store data from request
     * @return array
     */
    public function asArray(): array
    {
        return [
            "first_name" => $_POST['first_name'],
            "last_name" => $_POST['last_name'] ?? "",
            "phone_number" => $_POST['phone_number'],
            "country_code" => $_POST['country_code'] ?? "",
            "timezone_name" => $_POST['timezone_name'] ?? ""
        ];
    }
}