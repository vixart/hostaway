<?php

namespace App\Requests;

use App\Validations\PhoneBookFilterValidation;
use Phalcon\Di\Injectable;

class PhoneBookListRequest extends Injectable
{
    private PhoneBookFilterValidation $phoneBookFilterValidator;

    public function __construct()
    {
        $this->phoneBookFilterValidator = $this->di->get('phone_book_filter_validator');
    }

    /**
     * Validate phone book list request
     * @return PhoneBookListRequest
     */
    public function validate(): PhoneBookListRequest
    {
        $this->phoneBookFilterValidator->validate($_GET);
        return $this;
    }

    /**
     * Return array of filter's parameters
     * @return array
     */
    public function asArray(): array
    {
        return [
            'name' => $this->request->get('name', 'string', "")
        ];
    }
}