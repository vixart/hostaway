<?php
declare(strict_types=1);

use App\Resources\ExceptionResource;
use Phalcon\Di\FactoryDefault;
use Phalcon\Http\Response;
use Phalcon\Mvc\Application;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {
    /**
     * The FactoryDefault Dependency Injector automatically registers
     * the services that provide a full stack framework.
     */
    $di = new FactoryDefault();

    /**
     * Read services
     */
    include APP_PATH . '/config/services.php';

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();

    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/loader.php';

    /**
     * Handle routes
     */
    include APP_PATH . '/config/router.php';

    include __DIR__ . '/../vendor/autoload.php';

    /**
     * Handle the request
     */
    $application = new Application($di);

    echo $application->handle($_SERVER['REQUEST_URI'])->getContent();
}  catch (Exception $e) {
    $errorResource = new ExceptionResource();
    $response = new Response();
    $response->setStatusCode(400);
    $response->setJsonContent($errorResource->toArray($e), JSON_PRETTY_PRINT);
    $response->send();
    die;
}
