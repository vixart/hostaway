# Phalcon RESTful API

Phalcon base application. Represent simple RESTful api implementation. 

## Deployment

* composer install
* change database config in app/config/config.php
* vendor/bin/phalcon-migrations run
* php -S localhost:8000 -t public .htrouter.php

## Should add or fix

1) In the folder "requests" i made classes to validate and return validated data from request. This classes should use another layer "Entites"(business logic data structures) and return them. Right now its not explicit for other part of application what i return from asArray() methods.

2) Should add "Service" layer when business logic start to grown up.

3) Should add some abstract layers with interfaces. To use them in business logic instead of framework's classes. For example use interfaces for models and return them from repositories, so repository's user would have access to those methods which we provide in interface without public properties and phalcon model's methods.

4) Should add "Response" layer to my guzzle wrapper. And return them after request was send. To show only those methods which you expect to see after sending a request.
